<?php

use Phinx\Migration\AbstractMigration;

class AlteracaoAtividades extends AbstractMigration
{
    public function change()
    {
    	$this->table('atividades')
            ->addColumn('carga_horaria', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->update();
    }
}
