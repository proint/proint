<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
class CursosController extends AppController
{


    public function index()
    {
        $cursos = $this->Cursos->find()->where(['Cursos.status' => 1])->all();

        $this->set(compact('cursos'));
        $this->set('_serialize', ['cursos']);
    }

    public function view($id = null)
    {
        $curso = $this->Cursos->get($id, [
            'contain' => ['Categorias', 'Users']
        ]);

        $this->set('curso', $curso);
        $this->set('_serialize', ['curso']);
    }

     public function add()
    {
        $curso = $this->Cursos->newEntity();
        if ($this->request->is('post')) {
            $curso = $this->Cursos->patchEntity($curso, $this->request->getData());
            $curso->status = 1;
            $curso->criado_por = $this->retornarIdUsuarioAtivo();
            $curso->modificado_por = $this->retornarIdUsuarioAtivo();

            if ($this->Cursos->save($curso)) {
                $cursoCategoriaTable = TableRegistry::get('CursosCategorias');
                foreach ($curso->cat as $key => $value) {
                    if(!empty($value)){
                       $cursoCategoria = $cursoCategoriaTable->newEntity();
                       $cursoCategoria->curso_id = $curso->id;
                       $cursoCategoria->categoria_id = $value;
                       $cursoCategoriaTable->save($cursoCategoria);
                    }
                }
                $this->Flash->success(__('Curso criado com sucesso.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O curso não pôde ser criado, informe ao administrador.'));
            return $this->redirect(['action' => 'index']);
        }
        $categorias = $this->Cursos->Categorias->find('list', ['limit' => 200]);
        $this->set(compact('curso', 'categorias'));
        $this->set('_serialize', ['curso']);
    }

    public function edit($id = null)
    {
        $curso = $this->Cursos->get($id, [
            'contain' => ['Categorias']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $curso = $this->Cursos->patchEntity($curso, $this->request->getData());
            $curso->modificado_por = $this->retornarIdUsuarioAtivo();
            if ($this->Cursos->save($curso)) {
                $cursoCategoriaTable = TableRegistry::get('CursosCategorias');
                $cursoCategoriaDelete = $cursoCategoriaTable->find()->where(['CursosCategorias.curso_id' => $curso->id])->all();
                if(!empty($cursoCategoriaDelete)){
                    foreach ($cursoCategoriaDelete as $key => $value) {
                        $cursoCategoriaTable->delete($value);
                    }
                }
                foreach ($curso->cat as $key => $value) {
                    if(!empty($value)){
                       $cursoCategoria = $cursoCategoriaTable->newEntity();
                       $cursoCategoria->curso_id = $curso->id;
                       $cursoCategoria->categoria_id = $value;
                       $cursoCategoriaTable->save($cursoCategoria);
                    }
                }
                $this->Flash->success(__('Curso editado com sucesso'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O curso não pôde ser editado, informe ao administrador.'));
            return $this->redirect(['action' => 'index']);
        }

        $selected = array();
        foreach ($curso->categorias as $key => $value) {
            $selected[] = $value->id;
        }
        $categorias = $this->Cursos->Categorias->find('list', ['limit' => 200]);
        $this->set(compact('curso', 'categorias','selected'));
        $this->set('_serialize', ['curso']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $curso = $this->Cursos->get($id);
        $curso->status = 0;
        $curso->modificado_por = $this->retornarIdUsuarioAtivo();
        if ($this->Cursos->save($curso)) {
            $this->Flash->success(__('Curso excluído com sucesso.'));
        } else {
            $this->Flash->error(__('O curso não pôde ser excluído, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
