<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\ORM\TableRegistry;
class AtividadesController extends AppController
{

    public function index()
    {
        if($this->retornarIdUsuarioAtivo() == 1 || $this->retornarIdUsuarioAtivo() == 3)
            $atividades = $this->Atividades->find()->contain(['Categorias','Users'])->where(['Atividades.status >' => 0])->all();
        else{
            $atividades = $this->Atividades->find()->contain(['Categorias','Users'])->where(['Atividades.status >' => 0,'Atividades.user_id' => $this->retornarIdUsuarioAtivo()])->all();
        }
        $this->set(compact('atividades'));
        $this->set('_serialize', ['atividades']);
    }

    public function view($id = null)
    {
        $atividade = $this->Atividades->get($id, [
            'contain' => ['Categorias', 'Users']
        ]);

        $this->set('atividade', $atividade);
        $this->set('_serialize', ['atividade']);
    }

    public function add()
    {
        $atividade = $this->Atividades->newEntity();
        if ($this->request->is('post')) {
            //pr($this->request->data);exit;
            if($this->request->data['upload']['size'] < 10000000 && ($this->request->data['upload']['type'] == 'application/pdf' || $this->request->data['upload']['type'] == 'image/jpeg' || $this->request->data['upload']['type'] == 'image/png')){
                $userTable = TableRegistry::get('Users');
                $usuario = $userTable->find()->where(['Users.id' => $this->retornarIdUsuarioAtivo()])->first();
                $atividade = $this->Atividades->patchEntity($atividade, $this->request->getData());
                $atividade->user_id = $this->retornarIdUsuarioAtivo();
                $atividade->status = 1;
                $atividade->criado_por = $this->retornarIdUsuarioAtivo();;
                $atividade->modificado_por = $this->retornarIdUsuarioAtivo();;
                $tmp_dir = $this->request->data['upload']['tmp_name'];
                $dir = WWW_ROOT. media . DS .$usuario->username .DS;
                $this->checarDiretorio($dir);
                $atividade->upload = $this->moverArquivos($this->request->data['upload'],$dir);
                if ($this->Atividades->save($atividade)) {
                    $this->Flash->success(__('Atividade salva com sucesso!'));
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The atividade could not be saved. Please, try again.'));
                return $this->redirect(['action' => 'index']);
            }else{
                $this->Flash->error(__('O arquivo é maior que o limite concedido ou não está no formato permitido, a atividade não foi salva.'));
                return $this->redirect(['action' => 'index']);
            }
        }
        $categorias = $this->Atividades->Categorias->find('list', ['limit' => 200]);
        $users = $this->Atividades->Users->find('list', ['limit' => 200]);
        $this->set(compact('atividade', 'categorias', 'users'));
        $this->set('_serialize', ['atividade']);
    }

    public function edit($id = null)
    {
        $atividade = $this->Atividades->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $atividade = $this->Atividades->patchEntity($atividade, $this->request->getData());
            $atividade->modificado_por = $this->retornarIdUsuarioAtivo();
            if ($this->Atividades->save($atividade)) {
                $this->Flash->success(__('The atividade has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The atividade could not be saved. Please, try again.'));
            return $this->redirect(['action' => 'index']);
        }
        $categorias = $this->Atividades->Categorias->find('list', ['limit' => 200]);
        $users = $this->Atividades->Users->find('list', ['limit' => 200]);
        $this->set(compact('atividade', 'categorias', 'users'));
        $this->set('_serialize', ['atividade']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $atividade = $this->Atividades->get($id);
        $atividade->status = 0;
        $atividade->modificado_por =$this->retornarIdUsuarioAtivo();
        if ($this->Atividades->save($atividade)) {
            $this->Flash->success(__('Atividade inativada.'));
        } else {
            $this->Flash->error(__('A atividade não pôde ser excluída, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function checarDiretorio($dir){
        $folder = new Folder();
        if (!is_dir($dir)){
            $folder->create($dir);
        }
    }

    public function moverArquivos($arquivo, $dir)
    {
        $arquivo = $this->mudarNome($arquivo);
        $arquivoNovo = new File($arquivo['tmp_name']);
        $arquivoNovo->copy($dir.$arquivo['name']);
        $arquivoNovo->close();
        return $arquivo['name'];
    }

    public function mudarNome($arquivo){
        $arquivo_info = pathinfo($arquivo['name']);
        $novoNomeArquivo = md5(time()) .'.'.$arquivo_info['extension'];
        $arquivo['name'] = $novoNomeArquivo;
        return $arquivo;
    }

    public function avaliarAtividade()
    {
        $atividades = $this->Atividades->find()->contain(['Categorias','Users'])->where(['Atividades.status >' => 0])->order(['Atividades.status ASC'])->all();
        //pr($atividades);exit;
        $this->set(compact('atividades'));
        $this->set('_serialize', ['atividades']);
    }

    public function aprovarAtividade($id = null)
    {
        $atividade = $this->Atividades->get($id);
        $atividade->status = 2;
        $atividade->modificado_por = $this->retornarIdUsuarioAtivo();
        if ($this->Atividades->save($atividade)) {
            $this->Flash->success(__('Atividade aprovada.'));
        } else {
            $this->Flash->error(__('A atividade não pôde ser aprovada, tente novamente.'));
        }

        return $this->redirect(['action' => 'avaliarAtividade']);
    }

    public function reprovarAtividade($id = null)
    {
        $atividade = $this->Atividades->get($id);
        $atividade->status = 3;
        $atividade->modificado_por = $this->retornarIdUsuarioAtivo();
        if ($this->Atividades->save($atividade)) {
            $this->Flash->success(__('Atividade reprovada.'));
        } else {
            $this->Flash->error(__('A atividade não pôde ser reprovada, tente novamente.'));
        }

        return $this->redirect(['action' => 'avaliarAtividade']);
    }

    public function dashboardAluno(){
        $categoriasTable = TableRegistry::get('Categorias');
        $categoriasObj = $categoriasTable->find()->where(['Categorias.status' => 1])->all();
        $categorias = array();
        $arrayAtividades = array();
        $porcentagemAtividades = array();
        foreach ($categoriasObj as $key => $value) {
            $categorias['nome'][] = $value->nome;
            $categorias['horas_minima'][] = $value->quantidad_hora_minima;
            if(empty($arrayAtividades[$value->nome]))
                    $arrayAtividades[$value->nome] = 0;
            $atividades = $this->Atividades->find()->contain(['Categorias'])->where(['Atividades.status' => 2,'Atividades.user_id' => $this->retornarIdUsuarioAtivo(),'Atividades.categoria_id' => $value->id])->all();
            foreach ($atividades as $key2 => $value2) {
                    $arrayAtividades[$value->nome] +=  ($value2->carga_horaria*100)/$value->quantidad_hora_minima; 
                    if($arrayAtividades[$value->nome] > 100)
                        $arrayAtividades[$value->nome] = 100;
            }
        } 
        $media = 0;
        $count = 0;
        foreach ($arrayAtividades as $key => $value) {
            $porcentagemAtividades[] = $value;
            $media += $value; 
            $count++;
        }
        $media = $media / $count;

        $atividades = $this->Atividades->find()->select(['status'])->where(['Atividades.user_id' => $this->retornarIdUsuarioAtivo()])->all();
        $status = ['Pendente' =>0,'Deferido' => 0,'Indeferido' => 0];
        foreach ($atividades as $key => $value) {
            if($value->status == 1){
                $status['Pendente'] += 1;
            }else if($value->status == 2){
                $status['Deferido'] += 1;
            }else if($value->status == 3){
                $status['Indeferido'] += 1;
            }
        }
        //pr($status);exit;
        
        $this->set(compact('categorias','porcentagemAtividades','media','status'));
    }
}
