<?php
namespace App\Controller;

use App\Controller\AppController;
class MensagensController extends AppController
{

    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        //$mensagens = $this->Mensagens->find()->contain(['Users'])->where(['Mensagens.user_destino_id' => 1,'Mensagens.status >' => 0])->order(['Mensagens.created DESC'])->all();
        $mensagens = $this->Mensagens->find()->contain(['Users','UsuarioCriador'])->where(['Mensagens.status >' => 0,'Mensagens.user_destino_id' => $this->retornarIdUsuarioAtivo()])->all();
        //pr($mensagens);exit;
        $this->set(compact('mensagens'));
        $this->set('_serialize', ['mensagens']);
    }

    public function view($id = null)
    {
        $mensagen = $this->Mensagens->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('mensagen', $mensagen);
        $this->set('_serialize', ['mensagen']);
    }

    public function add()
    {
        $mensagem = $this->Mensagens->newEntity();
        if ($this->request->is('post')) {
            $mensagem = $this->Mensagens->patchEntity($mensagem, $this->request->getData());
            $mensagem->status = 1;
            $mensagem->criado_por = $this->retornarIdUsuarioAtivo();
            $mensagem->modificado_por = $this->retornarIdUsuarioAtivo();
            
            if ($this->Mensagens->save($mensagem)) {
                $this->Flash->success(__('Mensagem salva.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro ao enviar mensagem.'));
        }
        $userDestinos = $this->Mensagens->Users->find('list')->where(['Users.status' => 1]);
        $this->set(compact('mensagem', 'userDestinos'));
        $this->set('_serialize', ['mensagem']);
    }

    public function edit($id = null)
    {
        $mensagem = $this->Mensagens->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mensagem = $this->Mensagens->patchEntity($mensagem, $this->request->getData());
            $mensagem->modificado_por = $this->retornarIdUsuarioAtivo();
            if ($this->Mensagens->save($mensagem)) {
                $this->Flash->success(__('Mensagem salva.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro ao enviar mensagem'));
        }
        $userDestinos = $this->Mensagens->Users->find('list')->where(['Users.id <>'=> 1]);
        $this->set(compact('mensagem', 'userDestinos'));
        $this->set('_serialize', ['mensagem']);
    }

 
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $mensagen = $this->Mensagens->get($id);
        $mensagen->status = 0;
        $mensagem->modificado_por = $this->retornarIdUsuarioAtivo();
        if ($this->Mensagens->save($mensagen)) {
            $this->Flash->success(__('Mensagem apagada.'));
        } else {
            $this->Flash->error(__('Erro ao apagar Mensagem.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function enviadas()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $mensagens = $this->Mensagens->find()->contain(['Users','UsuarioCriador'])->where(['Mensagens.criado_por' => $this->retornarIdUsuarioAtivo(),'Mensagens.status >'=> 0])->order(['Mensagens.created DESC'])->all();

        $this->set(compact('mensagens'));
        $this->set('_serialize', ['mensagens']);
    }

    public function lerMensagem($id = null)
    {
        $mensagem = $this->Mensagens->get($id);
        $mensagem->status = 2;
        $mensagem->modificado_por = $this->retornarIdUsuarioAtivo();
        if ($this->Mensagens->save($mensagem)) {
            $this->Flash->success(__('Mensagem marcada como lida'));
        } else {
            $this->Flash->error(__('Erro ao alterar status da mensagem.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
