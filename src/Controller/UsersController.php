<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Auth\Auth;
use Cake\ORM\TableRegistry;

class UsersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['logout']);
    }

    public function index()
    {
        $users = $this->Users->find()->contain(['Cursos','Perfis'])->where(['Users.status' => 1])->all();
        
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

 
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Cursos', 'Perfis', 'Atividades']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->status = 1;
            $user->criado_por = $this->retornarIdUsuarioAtivo();
            $user->modificado_por = $this->retornarIdUsuarioAtivo();;
            $user->data_nascimento = trim(date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['data_nascimento']))));
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário salvo.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro ao salvar usuário'));
            return $this->redirect(['action' => 'index']);
        }
        $cursos = $this->Users->Cursos->find('list', ['limit' => 200]);
        $perfis = $this->Users->Perfis->find('list', ['limit' => 200]);

        $this->set(compact('user', 'cursos', 'perfis'));
        $this->set('_serialize', ['user']);
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        $user->data_nascimento = trim(date('d/m/Y', strtotime(str_replace('-', '/', $user->data_nascimento))));
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->data_nascimento = trim(date('Y-m-d', strtotime(str_replace('/', '-', $this->request->data['data_nascimento']))));
            $user->modificado_por = $this->retornarIdUsuarioAtivo();;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Usuário salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Erro ao salvar usuário.'));
            return $this->redirect(['action' => 'index']);
        }
        $cursos = $this->Users->Cursos->find('list', ['limit' => 200]);
        $perfis = $this->Users->Perfis->find('list', ['limit' => 200]);


        $this->set(compact('user', 'cursos', 'perfis'));
        $this->set('_serialize', ['user']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        $user->status = 0;
        if ($this->Users->save($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function login(){
        $this->viewBuilder()->layout("inicial");
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Users','action'=>'index']);
            }else{
                //$this->Flash->error(__('Usuário ou senha inválida, tente novamente'));
                return $this->redirect(['controller' => 'Users','action'=>'login']);
            }
        }
    }

    public function logout(){
        return $this->redirect($this->Auth->logout());
    }
}
