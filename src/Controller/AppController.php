<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
class AppController extends Controller
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authError' => 'Usuário ou senha incorretos',
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username','password' => 'password']
                ]
            ],
            'loginRedirect' => [
                'controller' => 'Cursos',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'storage' => 'Session'
        ]);
        if(!empty($this->Auth->user('username'))){
            $this->set("mensagensDefault", $this->retornarMensagens());
            $this->set("userLogado", $this->retornarUserLogado());
        }else{
            //$this->Flash->error(__('Usuário ou senha inválida, tente novamente'));
        }
    }
    public function beforeFilter(Event $event){
        //$this->Auth->allow(['index','view','display','add','edit','delete','avaliarAtividade','reprovarAtividade','aprovarAtividade','enviadas','lerMensagem']);
        $this->Auth->allow(['display','login','logout']);
    }

    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    public function retornarMensagens(){
        $usuario = $this->retornarIdUsuarioAtivo();
        $mensagensTable = TableRegistry::get('Mensagens');
        $mensagens = $mensagensTable->find()->where(['Mensagens.user_destino_id' => $usuario])->limit(5)->all();
        //$mensagens = $mensagensTable->find()->contain(['Users','UsuarioCriador'])->where(['Mensagens.user_destino_id' => 1,'Mensagens.status' => 1])->limit(5)->all();
        return $mensagens;
    }

    public function retornarIdUsuarioAtivo(){
        return $this->Auth->user('id');
    }

    public function retornarUserLogado(){
        $usuario = $this->retornarIdUsuarioAtivo();
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->find()->where(['Users.id' => $usuario])->first();
        //pr($user);exit;
        return $user;
    }
}
