<?php
namespace App\Controller;

use App\Controller\AppController;

class PerfisController extends AppController
{

    public function index()
    {
        $perfis = $this->Perfis->find()->where(['Perfis.status' => 1])->all();

        $this->set(compact('perfis'));
        $this->set('_serialize', ['perfis']);
    }

    public function view($id = null)
    {
        $perfil = $this->Perfis->get($id, [
            'contain' => []
        ]);

        $this->set('perfil', $perfil);
        $this->set('_serialize', ['perfil']);
    }

    public function add()
    {
        $perfil = $this->Perfis->newEntity();
        if ($this->request->is('post')) {
            $perfil = $this->Perfis->patchEntity($perfil, $this->request->getData());
            $perfil->status = 1;
            $perfil->criado_por = $this->retornarIdUsuarioAtivo();
            $perfil->modificado_por = $this->retornarIdUsuarioAtivo();

            if ($this->Perfis->save($perfil)) {
                $this->Flash->success(__('Perfil criado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Perfil não foi criado, tente novamente.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('perfil'));
        $this->set('_serialize', ['perfil']);
    }

    public function edit($id = null)
    {
        $perfil = $this->Perfis->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $perfil = $this->Perfis->patchEntity($perfil, $this->request->getData());
            $perfil->modificado_por = $this->retornarIdUsuarioAtivo();
            if ($this->Perfis->save($perfil)) {
                $this->Flash->success(__('Perfil editado com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Perfil não foi editado, tente novamente.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set(compact('perfil'));
        $this->set('_serialize', ['perfil']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $perfil = $this->Perfis->get($id);
        $perfil->status = 0;
        $perfil->modificado_por = $this->retornarIdUsuarioAtivo();
        if ($this->Perfis->save($perfil)) {
            $this->Flash->success(__('Perfil excluído com sucesso.'));
        } else {
            $this->Flash->error(__('Perfil não excluído, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
