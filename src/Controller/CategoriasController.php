<?php
namespace App\Controller;

use App\Controller\AppController;

class CategoriasController extends AppController
{

    public function index()
    {
        $categorias = $this->Categorias->find()->where(['Categorias.status' => 1])->all();

        $this->set(compact('categorias'));
        $this->set('_serialize', ['categorias']);
    }

    public function view($id = null)
    {
        $categoria = $this->Categorias->get($id, [
            'contain' => ['Cursos', 'Atividades']
        ]);

        $this->set('categoria', $categoria);
        $this->set('_serialize', ['categoria']);
    }

    public function add()
    {
        $categoria = $this->Categorias->newEntity();
        if ($this->request->is('post')) {
            $categoria = $this->Categorias->patchEntity($categoria, $this->request->getData());
            $categoria->status = 1;
            $categoria->criado_por = $this->retornarIdUsuarioAtivo();
            $categoria->modificado_por = 1;
            if ($this->Categorias->save($categoria)) {
                $this->Flash->success(__('Categoria salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A categoria não pôde ser salva, tente novamente.'));
            return $this->redirect(['action' => 'index']);
        }
        $cursos = $this->Categorias->Cursos->find('list', ['limit' => 200]);
        $this->set(compact('categoria', 'cursos'));
        $this->set('_serialize', ['categoria']);
    }

    public function edit($id = null)
    {
        $categoria = $this->Categorias->get($id, [
            'contain' => ['Cursos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $categoria = $this->Categorias->patchEntity($categoria, $this->request->getData());
            $categoria->modificado_por = $this->retornarIdUsuarioAtivo();
            if ($this->Categorias->save($categoria)) {
                $this->Flash->success(__('Categoria salva com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A categoria não pôde ser salva, tente novamente.'));
            return $this->redirect(['action' => 'index']);
        }
        $cursos = $this->Categorias->Cursos->find('list', ['limit' => 200]);
        $this->set(compact('categoria', 'cursos'));
        $this->set('_serialize', ['categoria']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $categoria = $this->Categorias->get($id);
        $categoria->status = 0;
        $categoria->modificado_por = $this->retornarIdUsuarioAtivo();
        if ($this->Categorias->save($categoria)) {
            $this->Flash->success(__('Categoria excluída com sucesso'));
        } else {
            $this->Flash->error(__('Erro ao excluir a categoria, tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
