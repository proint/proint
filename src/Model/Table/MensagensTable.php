<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Mensagens Model
 *
 * @property \App\Model\Table\UserDestinosTable|\Cake\ORM\Association\BelongsTo $UserDestinos
 *
 * @method \App\Model\Entity\Mensagen get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mensagen newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mensagen[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mensagen|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mensagen patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mensagen[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mensagen findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MensagensTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('mensagens');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_destino_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('UsuarioCriador', [
            'className' => 'Users',
            'foreignKey' => 'criado_por',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('titulo', 'create')
            ->notEmpty('titulo');

        $validator
            ->requirePresence('mensagem', 'create')
            ->notEmpty('mensagem');

        $validator
            ->integer('status')
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->integer('criado_por')
            ->requirePresence('criado_por', 'create')
            ->notEmpty('criado_por');

        $validator
            ->integer('modificado_por')
            ->requirePresence('modificado_por', 'create')
            ->notEmpty('modificado_por');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_destino_id'], 'Users'));

        return $rules;
    }
}
