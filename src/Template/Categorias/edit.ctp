<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Adicionar Categoria</h4>
</div>
<?php echo $this->Form->create($categoria,['role'=>'form', 'id'=>'FormEditCategoria','method'=>'post']); ?>
<div class="modal-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?= $this->Flash->render() ?>
        <div class="clearfix"></div>
    </div> 
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->input('nome', ['class'=>'form-control', 'id'=>'nome']); ?>
            <?php echo $this->Form->input('descricao', ['class'=>'form-control', 'id'=>'descricao']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('quantidad_hora_minima', ['class'=>'form-control', 'id'=>'quantidad_hora_minima','type' => 'number','label' => 'Quantidade de Horas Mínima']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('quantidad_hora_maxima', ['class'=>'form-control', 'id'=>'quantidad_hora_maxima','type' => 'number','label' => 'Quantidade de Horas Máxima']); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->button('Salvar',['class'=>'btn btn-success btn-sm']); ?>
            <?php echo $this->Form->button('Limpar',['type'=>'reset', 'class'=>'btn btn-warning btn-sm']); ?>
        </div>
    </div>  
</div>
<?php echo $this->Form->end(); ?>
<script>
    $(function () { 

        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
        $("#FormEditCategoria").submit(function(){
            if(!validarForm()){
                return false;
            }else{
                return true;
            }
        });
    });

    function validarForm(){
        if($("#nome").val() == ""){
            alert("Preencha o nome da categoria!");
            return false;
        }else if($("#descricao").val() == ""){
            alert("Preencha a descrição da categoria!");
            return false;
        }else if($("#quantidad_hora_minima").val() == ""){
            alert("Preencha a quantidade de horas mínima dessa categoria!");
            return false;
        }else if($("#quantidad_hora_maxima").val() == ""){
            alert("Preencha a quantidade de horas máxima complementares dessa categoria!");
            return false;
        }else{
            return true;
        }
    }
</script>

