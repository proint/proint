<?php echo $this->Html->css('dataTables.bootstrap.css') ?>
<?php echo $this->Html->css('responsive.bootstrap.css') ?>
<?php echo $this->Html->css('buttons.bootstrap.css') ?>
<?php echo $this->Html->css('fixedHeader.bootstrap.css') ?>
<?php echo $this->Html->css('scroller.bootstrap.css') ?>

<div class="page-title">
    <div class="title_left">
        <h3>Cursos</h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php echo $this->Html->link('Adicionar', ['action'=>'add'], ['escape'=>false,'class' => 'btn btn-success btn-sm', 'data-toggle'=>'modal','data-target'=>'#AddCurso']); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?= $this->Flash->render() ?>
                    <div class="clearfix"></div>
                </div> 
                <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                    <thead>
                        <tr role="row">
                            <th>Id</th>
                            <th>Nome</th>
                            <th>Descrição</th>
                            <th>Períodos</th>
                            <th>Horas complementares</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($cursos as $curso): ?>
                            <tr>
                                <td><?php echo $curso->id; ?></td>
                                <td><?php echo $curso->nome; ?></td>
                                <td><?php echo $curso->descricao; ?></td>
                                <td><?php echo $curso->quantidade_periodo; ?></td>
                                <td><?php echo $curso->quantidade_hora_complementar; ?></td>
                                <td>
                                    <?php echo $this->Html->link('Editar', ['action'=>'edit',$curso->id], ['escape'=>false,'class' => 'btn btn-warning btn-xs', 'data-toggle'=>'modal','data-target'=>'#EditCurso']); ?>
                                    <?php echo $this->Form->postLink('Excluir',['action' => 'delete', $curso->id], ['type'=>'button','class' => 'btn btn-danger btn-xs', 'escape' => false,'confirm' =>'Tem certeza que deseja excluir esse curso?']); ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-hidden="true"  id="AddCurso">
    <div class="modal-dialog">
      <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-hidden="true"  id="EditCurso">
    <div class="modal-dialog">
      <div class="modal-content"></div>
    </div>
</div>
<?php echo $this->Html->script('jquery.dataTables') ?>
<?php echo $this->Html->script('dataTables.bootstrap') ?>
<?php echo $this->Html->script('dataTables.responsive') ?>
<?php echo $this->Html->script('responsive.bootstrap') ?>
