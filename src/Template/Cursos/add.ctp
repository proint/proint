<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Adicionar Curso</h4>
</div>
<?php echo $this->Form->create($curso,['role'=>'form', 'id'=>'FormAddCurso','method'=>'post']); ?>
<div class="modal-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?= $this->Flash->render() ?>
        <div class="clearfix"></div>
    </div> 
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->input('nome', ['class'=>'form-control', 'id'=>'nome']); ?>
            <?php echo $this->Form->input('descricao', ['class'=>'form-control', 'id'=>'descricao']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('quantidade_periodo', ['class'=>'form-control', 'id'=>'quantidade_periodo','type' => 'number','label' => 'Quantidade de Períodos']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('quantidade_hora_complementar', ['class'=>'form-control', 'id'=>'quantidade_hora_complementar','type' => 'number','label' => 'Horas Complementares']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('categoria', ['class'=>'checkbox icheckbox_flat-green checked', 'id'=>'categoria','multiple' => 'checkbox' ,'options' => $categorias,'name' => 'cat']); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->button('Salvar',['class'=>'btn btn-success btn-sm']); ?>
            <?php echo $this->Form->button('Limpar',['type'=>'reset', 'class'=>'btn btn-warning btn-sm']); ?>
        </div>
    </div>  
</div>
<?php echo $this->Form->end(); ?>
<script>
    $(function () { 

        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
        $("#FormAddCurso").submit(function(){
            if(!validarForm()){
                return false;
            }else{
                return true;
            }
        });
    });

    function validarForm(){
        if($("#nome").val() == ""){
            alert("Preencha o nome do curso!");
            return false;
        }else if($("#descricao").val() == ""){
            alert("Preencha a descrição do curso!");
            return false;
        }else if($("#quantidade_periodo").val() == ""){
            alert("Preencha a quantidade de períodos do curso!");
            return false;
        }else if($("#quantidade_hora_complementar").val() == ""){
            alert("Preencha a quantidade de horas complementares do curso!");
            return false;
        }else if($("#categoria").val() == ""){
            alert("Preencha a categoria do curso!");
            return false;
        }else{
            return true;
        }
    }
</script>

