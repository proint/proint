<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Adicionar Perfil</h4>
</div>
<?php echo $this->Form->create($perfil,['role'=>'form', 'id'=>'FormAddPerfil','method'=>'post']); ?>
<div class="modal-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?= $this->Flash->render() ?>
        <div class="clearfix"></div>
    </div> 
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('nome', ['class'=>'form-control', 'id'=>'nome']); ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <?php echo $this->Form->input('alias', ['class'=>'form-control', 'id'=>'alias']); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->button('Salvar',['class'=>'btn btn-success btn-sm']); ?>
            <?php echo $this->Form->button('Limpar',['type'=>'reset', 'class'=>'btn btn-warning btn-sm']); ?>
        </div>
    </div>  
</div>
<?php echo $this->Form->end(); ?>

<script>
    $(function () { 

        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
        $("#FormAddPerfil").submit(function(){
            if(!validarForm()){
                return false;
            }else{
                return true;
            }
        });
    });

    function validarForm(){
        if($("#nome").val() == ""){
            alert("Preencha o nome do perfil!");
            return false;
        }else if($("#alias").val() == ""){
            alert("Preencha o apelido do perfil!");
            return false;
        }else{
            return true;
        }
    }
</script>

