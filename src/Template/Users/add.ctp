<?= $this->Html->css('font-awesome.css') ?>
<?= $this->Html->css('nprogress.css') ?>
<?= $this->Html->css('datepicker.css') ?>

<?php echo $this->Html->script('input-maskmoney') ?>
<?php echo $this->Html->script('bootstrap-datepicker') ?>
<?php echo $this->Html->script('jquery.smartWizard') ?>
<?php echo $this->Html->script('nprogress') ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Adicionar Usuário</h4>
</div>
<?php echo $this->Form->create($user,['role'=>'form', 'id'=>'FormAddUser','method'=>'post']); ?>
<div class="modal-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?= $this->Flash->render() ?>
        <div class="clearfix"></div>
    </div> 
    <div class="row">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Form->input('nome', ['class'=>'form-control', 'id'=>'nome']); ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Form->input('cpf', ['class'=>'form-control','id' => 'cpf','data-mask'=>'999.999.999-99', 'label' => 'Cpf','type' => 'text']); ?>
            </div>
           
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Form->input('email', ['class'=>'form-control', 'id'=>'email','type' => 'email']); ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Form->input('data_nascimento', ['label' => 'Data Nascimento','type'=>'text', 'class' => 'form-control','id'=>'dataNascimento','data-date-format'=>'dd/mm/yyyy']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Form->input('username', ['class'=>'form-control', 'id'=>'username','label' => 'Login']); ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Form->input('password', ['class'=>'form-control', 'label'=>'Senha','type' => 'password']); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Form->input('curso_id', ['class'=>'form-control', 'id'=>'curso','options' => $cursos,'type' => 'select','empty' => 'Selecione um curso']); ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php echo $this->Form->input('perfil_id', ['class'=>'form-control', 'id'=>'perfil','options' => $perfis,'type' => 'select','empty' => 'Selecione um perfil']); ?>
            </div>
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->button('Salvar',['class'=>'btn btn-success btn-sm']); ?>
            <?php echo $this->Form->button('Limpar',['type'=>'reset', 'class'=>'btn btn-warning btn-sm']); ?>
        </div>
    </div>  
</div>
<?php echo $this->Form->end(); ?>


<script>
    $(function () { 
        $(document).ready(function () {
            $('#dataNascimento').datepicker().on('changeDate', function(ev)
            {                 
                 $('.datepicker').hide();
            });
        }); 
        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
        $("#FormAddUser").submit(function(){
            if(!validarForm()){
                return false;
            }else{
                return true;
            }
        });
    });

    function validarForm(){
        if($("#nome").val() == ""){
            alert("Preencha o nome do usuário!");
            return false;
        }else if($("#cpf").val() == ""){
            alert("Preencha o cpf do usuário!");
            return false;
        }else if($("#dataNascimento").val() == ""){
            alert("Preencha a data de nascimento do usuário!");
            return false;
        }else if($("#email").val() == ""){
            alert("Preencha o email do usuário!");
            return false;
        }else if($("#username").val() == ""){
            alert("Preencha o login do usuário!");
            return false;
        }else if($("#password").val() == ""){
            alert("Preencha a senha do usuário!");
            return false;
        }else if($("#curso").val() == ""){
            alert("Preencha o curso do usuário!");
            return false;
        }else if($("#perfil").val() == ""){
            alert("Preencha o perfil do usuário!");
            return false;
        }else{
            return true;
        }
    }
</script>

