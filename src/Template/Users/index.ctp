<?php echo $this->Html->css('dataTables.bootstrap.css') ?>
<?php echo $this->Html->css('responsive.bootstrap.css') ?>
<?php echo $this->Html->css('buttons.bootstrap.css') ?>
<?php echo $this->Html->css('fixedHeader.bootstrap.css') ?>
<?php echo $this->Html->css('scroller.bootstrap.css') ?>

<div class="page-title">
    <div class="title_left">
        <h3>Usuários</h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php echo $this->Html->link('Adicionar', ['action'=>'add'], ['escape'=>false,'class' => 'btn btn-success btn-sm', 'data-toggle'=>'modal','data-target'=>'#AddUser']); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?= $this->Flash->render() ?>
                    <div class="clearfix"></div>
                </div> 
                <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                    <thead>
                        <tr role="row">
                            <th>Id</th>
                            <th>Nome</th>
                            <th>Login</th>
                            <th>Cpf</th>
                            <th>Curso</th>
                            <th>Perfil</th>
                            <th>Data Nascimento</th>
                            <th>Email</th>
                            <th style="width: 100px;">Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user): ?>
                            <tr>
                                <td><?php echo $user->id; ?></td>
                                <td><?php echo $user->nome; ?></td>
                                <td><?php echo $user->login; ?></td>
                                <td><?php echo $user->cpf; ?></td>
                                <td><?php echo $user->curso->nome; ?></td>
                                <td><?php echo $user->perfi->nome; ?></td>
                                <td><?php 
                                        if(!empty($user->data_nascimento)){
                                            echo $user->data_nascimento->format('d/m/Y');
                                        } 
                                    ?>        
                                </td>
                                <td><?php echo $user->email; ?></td>
                                <td>
                                    <?php echo $this->Html->link('Editar', ['action'=>'edit',$user->id], ['escape'=>false,'class' => 'btn btn-warning btn-xs', 'data-toggle'=>'modal','data-target'=>'#EditUser']); ?>
                                    <?php 
                                        if($user->id != 1)
                                            echo $this->Form->postLink('Excluir',['action' => 'delete', $user->id], ['type'=>'button','class' => 'btn btn-danger btn-xs', 'escape' => false,'confirm' =>'Tem certeza que deseja excluir esse usuário?']); 
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-hidden="true"  id="AddUser">
    <div class="modal-dialog">
      <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-hidden="true"  id="EditUser">
    <div class="modal-dialog">
      <div class="modal-content"></div>
    </div>
</div>
<?php echo $this->Html->script('jquery.dataTables') ?>
<?php echo $this->Html->script('dataTables.bootstrap') ?>
<?php echo $this->Html->script('dataTables.responsive') ?>
<?php echo $this->Html->script('responsive.bootstrap') ?>
