<div class="logo_section">
  <div style="background-color:#1ABB9C">
      <center><?php echo $this->Html->image('logo.jpg', ['width'=>'300px','heigth'=>'300px']); ?></center>
  </div>
</div>
<div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content">
            <?= $this->Form->create() ?>
                <div>
                    <center><?php //echo $this->Html->image('logo.png', ['width'=>'150px']); ?></center>
                </div>
                <br>
                <div>
                    <?= $this->Form->input('username',['class'=>'form-control', 'placeholder'=>'Login','label'=>false,'style'=>'text-align:center']) ?>
                </div>
                <div>
                    <?= $this->Form->input('password',['class'=>'form-control', 'placeholder'=>'Senha','label'=>false, 'type'=>'password', 'style'=>'text-align:center']); ?>
                </div>
                <div class="clearfix"></div>
                <div>
                    <?= $this->Form->button('Entrar',['class'=>'btn btn-block btn-primary','style'=>'background-color:#1ABB9C; border:none;']) ?>
                </div>
            <?= $this->Form->end(); ?>
        </section>
    </div>
</div>
