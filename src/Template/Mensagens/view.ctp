<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Mensagen $mensagen
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Mensagen'), ['action' => 'edit', $mensagen->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Mensagen'), ['action' => 'delete', $mensagen->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mensagen->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Mensagens'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Mensagen'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="mensagens view large-9 medium-8 columns content">
    <h3><?= h($mensagen->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Titulo') ?></th>
            <td><?= h($mensagen->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($mensagen->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User Destino Id') ?></th>
            <td><?= $this->Number->format($mensagen->user_destino_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= $this->Number->format($mensagen->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Criado Por') ?></th>
            <td><?= $this->Number->format($mensagen->criado_por) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modificado Por') ?></th>
            <td><?= $this->Number->format($mensagen->modificado_por) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($mensagen->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($mensagen->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Mensagem') ?></h4>
        <?= $this->Text->autoParagraph(h($mensagen->mensagem)); ?>
    </div>
</div>
