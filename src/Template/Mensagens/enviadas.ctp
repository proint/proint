<?php echo $this->Html->css('dataTables.bootstrap.css') ?>
<?php echo $this->Html->css('responsive.bootstrap.css') ?>
<?php echo $this->Html->css('buttons.bootstrap.css') ?>
<?php echo $this->Html->css('fixedHeader.bootstrap.css') ?>
<?php echo $this->Html->css('scroller.bootstrap.css') ?>

<div class="page-title">
    <div class="title_left">
        <h3>Mensagens</h3>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?php echo $this->Html->link('Adicionar', ['action'=>'add'], ['escape'=>false,'class' => 'btn btn-success btn-sm', 'data-toggle'=>'modal','data-target'=>'#AddMensagem']); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <?= $this->Flash->render() ?>
                    <div class="clearfix"></div>
                </div> 
                <table id="datatable" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="datatable_info">
                    <thead>
                        <tr role="row">
                            <th>Id</th>
                            <th>Título</th>
                            <th>Mensagem</th>
                            <th>Destinatário</th>
                            <th>Situação</th>
                            <th>Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($mensagens as $mensagem): ?>
                            <tr>
                                <td><?php echo $mensagem->id; ?></td>
                                <td><?php echo $mensagem->titulo; ?></td>
                                <td><?php echo $mensagem->mensagem; ?></td>
                                <td><?php echo $mensagem->user->nome; ?></td>
                                <td><?php 
                                        if($mensagem->status == 1)
                                            echo "Não lida";
                                        else if($mensagem->status == 2)
                                            echo "Lida";
                                    ?>    
                                </td>
                                <td>
                                    <?php if($mensagem->status == 1){
                                            echo $this->Html->link('Editar', ['action'=>'edit',$mensagem->id], ['escape'=>false,'class' => 'btn btn-warning btn-xs', 'data-toggle'=>'modal','data-target'=>'#EditMensagem']);
                                            echo $this->Form->postLink('Excluir',['action' => 'delete', $mensagem->id], ['type'=>'button','class' => 'btn btn-danger btn-xs', 'escape' => false,'confirm' =>'Tem certeza que deseja excluir essa mensagem?']); 
                                        }
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-hidden="true"  id="AddMensagem">
    <div class="modal-dialog">
      <div class="modal-content"></div>
    </div>
</div>
<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-hidden="true"  id="EditMensagem">
    <div class="modal-dialog">
      <div class="modal-content"></div>
    </div>
</div>
<?php echo $this->Html->script('jquery.dataTables') ?>
<?php echo $this->Html->script('dataTables.bootstrap') ?>
<?php echo $this->Html->script('dataTables.responsive') ?>
<?php echo $this->Html->script('responsive.bootstrap') ?>
