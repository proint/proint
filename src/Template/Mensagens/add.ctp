<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Enviar Mensagem</h4>
</div>
<?php echo $this->Form->create($mensagem,['role'=>'form', 'id'=>'FormAddMensagem','method'=>'post']); ?>
<div class="modal-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?= $this->Flash->render() ?>
        <div class="clearfix"></div>
    </div> 
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->input('titulo', ['class'=>'form-control', 'id'=>'titulo']); ?>
            <?php echo $this->Form->input('mensagem', ['class'=>'form-control', 'id'=>'mensagem']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('user_destino_id', ['class'=>'form-control', 'options' => $userDestinos,'type' => 'select']); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->button('Salvar',['class'=>'btn btn-success btn-sm']); ?>
            <?php echo $this->Form->button('Limpar',['type'=>'reset', 'class'=>'btn btn-warning btn-sm']); ?>
        </div>
    </div>  
</div>
<?php echo $this->Form->end(); ?>
<script>
    $(function () { 

        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
        $("#FormAddMensagem").submit(function(){
            if(!validarForm()){
                return false;
            }else{
                return true;
            }
        });
    });

    function validarForm(){
        if($("#titulo").val() == ""){
            alert("Preencha o titulo da mensagem!");
            return false;
        }else if($("#mensagem").val() == ""){
            alert("Digite uma mensagem!");
            return false;
        }else{
            return true;
        }
    }
</script>

