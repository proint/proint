<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Adicionar Atividade</h4>
</div>
<?php echo $this->Form->create($atividade,['role'=>'form', 'id'=>'FormAddAtividade','method'=>'post','type'=>'file']); ?>
<div class="modal-body">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <?= $this->Flash->render() ?>
        <div class="clearfix"></div>
    </div> 
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-12">
            <?php echo $this->Form->input('nome', ['class'=>'form-control', 'id'=>'nome']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('carga_horaria', ['class'=>'form-control', 'type'=>'number','label' => 'Horas da Atividade','min' => '1','pattern' => '[0-9]+$']); ?>
        </div>
        
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <?php echo $this->Form->input('descricao', ['class'=>'form-control', 'id'=>'descricao']); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('categoria_id', ['class'=>'form-control','type' => 'select' ,'options' => $categorias]); ?>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <?php echo $this->Form->input('upload', ['type'=>'file','label'=>'Arquivo']); ?>
        </div>
    </div>
</div>
<div class="modal-footer">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <label>O arquivo deve ter menos de 10 mb e deve estar em pdf, png ou jpeg!</label>
            <?php echo $this->Form->button('Salvar',['class'=>'btn btn-success btn-sm']); ?>
            <?php echo $this->Form->button('Limpar',['type'=>'reset', 'class'=>'btn btn-warning btn-sm']); ?>
        </div>
    </div>  
</div>
<?php echo $this->Form->end(); ?>
<script>
    $(document).ready(function () {

        $('body').on('hidden.bs.modal', '.modal', function () {
            $(this).removeData('bs.modal');
        });
        $("#FormAddAtividade").submit(function(){
            if(!validarForm()){
                return false;
            }else{
                return true;
            }
        });
    });

    function validarForm(){
        if($("#nome").val() == ""){
            alert("Preencha o nome da atividade!");
            return false;
        }else if($("#descricao").val() == ""){
            alert("Preencha a descrição da atividade!");
            return false;
        }else if($("#carga_horaria").val() == "" || $("#carga_horaria").val() < 1){
            alert("Preencha a carga horária corretamente, ela deve ser no mínimo 1 hora!");
            return false;
        }else{
            return true;
        }
    }
</script>