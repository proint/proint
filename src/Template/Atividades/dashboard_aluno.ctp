<?php echo $this->Html->css('bootstrap-progressbar-3.3.4.css') ?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><i class="fa fa-dashboard"></i> Dashboard</h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
    		<div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              	<span class="count_top"><h3><?php echo number_format($media,2) ."%"; ?> <i class="fa fa-smile-o"></i></h3></span>
              	<div class="count"><h4> Concluído</h4></div>
              	<span class="count_bottom"><i>Carga horária mínima total preenchida</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              	<span class="count_top"><h3><?php echo $status['Pendente']; ?> <i class="fa fa-list"></i></h3></span>
              	<div class="count"><h4> Pendentes</h4></div>
              	<span class="count_bottom"><i>Atividades submetidas para avaliação do coordenador</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              	<span class="count_top"><h3><?php echo $status['Deferido']?>   <i class="fa fa-thumbs-o-up"></i>	</h3></span>

              	<div class="count"><h4> Deferidas</h4></div>
              	<span class="count_bottom"><i>Atividades submetidas para avaliação do coordenador</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              	<span class="count_top"><h3><?php echo $status['Indeferido']; ?>   <i class="fa fa-thumbs-o-down"></i></h3></span>
              	<div class="count"><h4> Indeferidas</h4></div>
              	<span class="count_bottom"><i>Atividades submetidas para avaliação do coordenador</span>
            </div>
    	</div>
    	
    </div>
    <div class="row">
    	<div class="col-md-7 col-sm-7 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Carga Horária Cadastrada</h2>
                	<div class="clearfix"></div>
                </div>
            	<div class="x_content">
            		<table class="" style="width:100%">
	                    <tr>
	                      	<th style="width:37%;">
	                        	<p>Top 5</p>
	                      	</th>
	                      	<th>
	                        	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		                          <p class="">Categorias</p>
		                        </div>
		                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		                          <p class="">Progresso</p>
		                        </div>
		                    </th>
	                    </tr>
	                    <tr>
	                      	<td>
	                        	<canvas class='canvasDoughnut2' id="graficoPorCategoria" height="200" width="400" style="margin: 15px 10px 10px 0"></canvas>
	                      	</td>
	                      	<td>
	                        	<table class="tile_info">
	                        		<?php foreach ($categorias['nome'] as $key => $value) { ?>
	                        			<tr>
	                        				<td><?php echo $value; ?></td>
		                            		<td style="text-align: left;"><p><?php echo number_format($porcentagemAtividades[$key],2)."%"; ?></td>
		                          		</tr>
	                        		<?php } ?>
	                        	</table>
	                      	</td>
	                    </tr>
	                </table>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Por Categorias de Atividades</h2>
                	<div class="clearfix"></div>
                </div>
            	<div class="x_content">
            		<?php foreach ($categorias['nome'] as $key => $value) { ?>
            		<?php $valor = 'style="width:'.$porcentagemAtividades[$key]."%\""; //pr($valor);exit;?>
	                    <div class="widget_summary">
		                    <div class="w_left w_25">
		                      	<span><?php echo $value; ?></span>
		                    </div>
		                    <div class="w_center w_55">
		                      	<div class="progress">
		                        	<div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" <?php echo $valor; ?> >
		                          		<span class="sr-only">50% Complete</span>
		                        	</div>
		                      	</div>
		                    </div>
		                    <div class="w_right w_20">
		                      	<span><?php echo $categorias['horas_minima'][$key].' h'; ?></span>
		                    </div>
		                    <div class="clearfix"></div>
		                </div>
		            <?php } ?>
                </div>
            </div>
        </div>
    </div>

<?php echo $this->Html->script('Chart') ?>
<?php echo $this->Html->script('bootstrap-progressbar') ?>
<script>
	var colorGenerator = function () { 
	    return [
	    	"#1abc9c", // Turquoise
	    	"#2ecc71", // Emerald
	    	"#3498db", // Peter River
	    	"#9b59b6", // Amethyst
	    	"#34495e", // Wet Asphalt
	    	"#f1c40f", // Sun Flower
	    	"#e67e22", // Carrot
	    	"#e74c3c", // Alizarin
	    	"#ecf0f1", // Clouds
	    	"#95a5a6", // Concrete
	    	"#16a085", // Green Sea
	    	"#27ae60", // Nephritis
	    	"#2980b9", // Belize Hole
	    	"#8e44ad", // Wisteria
	    	"#2c3e50", // Midnight Blue
	    	"#f39c12", // Orange
	    	"#d35400", // Pumpkin
	    	"#c0392b", // Pomegranate
	    	"#bdc3c7", // Silver
	    	"#7f8c8d", // Asbestos
	    	"#ff1981", // Rosa
	    	"#aad450", // Vimeo (Verde)
	    	"#00405d", // About.me
	    ];
	};

	var adicionarCoresElemento = function(elemet){
		var arrayCores = [];
		var cores = colorGenerator();
		for (var i = 0; i < cores.length; i++) {
			arrayCores.push(cores[i]);
		}	
		return arrayCores;
	}	

	$(document).ready(function () {
		graficoPorCategoria();

	})

	function graficoPorCategoria(){
		var categorias = '<?php echo json_encode($categorias['nome']); ?>';
		var porcentagem = '<?php echo json_encode($porcentagemAtividades); ?>';
		var coresCategorias = adicionarCoresElemento(JSON.parse(categorias));
		var coresCategoriasBack = adicionarCoresElemento(JSON.parse(categorias));

		var chart_doughnut_settings = {
				type: 'doughnut',
				tooltipFillColor: "rgba(51, 51, 51, 0.55)",
				data: {
					labels: JSON.parse(categorias),
					datasets: [{
						data: JSON.parse(porcentagem),
						backgroundColor: coresCategorias,
						hoverBackgroundColor: coresCategoriasBack
					}]
				},
				options: { 
					legend: {
			    		data: JSON.parse(porcentagem),
			    		labels: {
			    			fontSize: 12,
			    		}
			    	}, 
					responsive: true 
				}
			}
		
			$('.canvasDoughnut2').each(function(){
				
				var chart_element = $(this);
				var chart_doughnut = new Chart( chart_element, chart_doughnut_settings);
				
			});			
	}
	
</script>