<?php

$cakeDescription = 'ATIVO';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('font-awesome.css') ?>
    <?= $this->Html->css('custom.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?php echo $this->Html->script('jquery') ?>
    <?php echo $this->Html->script('bootstrap') ?>
</head>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                       <!-- <a href="" class="site_title"><i class="fa fa-paw"></i> <span>ATIVO</span></a>-->
                       <center><?php echo $this->Html->image('ativo.jpg', ['width'=>'140px','heigth'=>'170px']); ?></center>
                    </div>
                    <div class="clearfix"></div>
                    <div class="profile clearfix">
                      <div class="profile_info">
                        <center><span>Bem vindo,</span>
                        <h2><?php echo $userLogado->nome; ?></h2></center>
                      </div>
                    </div>
                    <br />
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>Menu</h3>
                            <ul class="nav side-menu">
                            <?php if($userLogado->perfil_id == 1){ ?>
                            
                                <li><a><i class="fa fa-home"></i> Administrador <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                      <li><?php echo $this->Html->link('Cursos', ['action' => 'index', 'controller' => 'Cursos'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Perfis', ['action' => 'index', 'controller' => 'Perfis'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Usuários', ['action' => 'index', 'controller' => 'Users'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Categorias de Atividades', ['action' => 'index', 'controller' => 'Categorias'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Atividades', ['action' => 'index', 'controller' => 'Atividades'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Avaliar Atividades', ['action' => 'avaliarAtividade', 'controller' => 'Atividades'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Caixa de entrada', ['action' => 'index', 'controller' => 'Mensagens'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Mensagens Enviadas', ['action' => 'enviadas', 'controller' => 'Mensagens'], ['escape'=>false]) ?></li>
    
                                    </ul>
                                </li>
                            <?php }else if($userLogado->perfil_id == 2){ ?>

                                <li><a><i class="fa fa-home"></i> Aluno <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                      <li><?php echo $this->Html->link('Progesso', ['action' => 'dashboardAluno', 'controller' => 'Atividades'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Atividades', ['action' => 'index', 'controller' => 'Atividades'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Caixa de entrada', ['action' => 'index', 'controller' => 'Mensagens'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Mensagens Enviadas', ['action' => 'enviadas', 'controller' => 'Mensagens'], ['escape'=>false]) ?></li>
                                    </ul>
                                </li>
                            <?php }else if($userLogado->perfil_id == 3){ ?>
                                <li><a><i class="fa fa-home"></i> Coordenador <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu">
                                      <li><?php echo $this->Html->link('Progesso', ['action' => 'dashboardAluno', 'controller' => 'Atividades'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Atividades', ['action' => 'index', 'controller' => 'Atividades'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Usuários', ['action' => 'index', 'controller' => 'Users'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Categorias de Atividades', ['action' => 'index', 'controller' => 'Categorias'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Avaliar Atividades', ['action' => 'avaliarAtividade', 'controller' => 'Atividades'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Caixa de entrada', ['action' => 'index', 'controller' => 'Mensagens'], ['escape'=>false]) ?></li>
                                      <li><?php echo $this->Html->link('Mensagens Enviadas', ['action' => 'enviadas', 'controller' => 'Mensagens'], ['escape'=>false]) ?></li>
                                      
                                    </ul>
                                </li>
                            <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>

                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <?php echo $userLogado->nome; ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><?= $this->Html->link('Sair', ['controller' => 'Users', 'action' => 'logout'],  array('escape' => false)) ?>
                                    </li>
                                </ul>
                            </li>

                            <li role="presentation" class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="badge bg-green"></span>
                                </a>
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                    <?php if(!empty($mensagensDefault)){
                                            foreach ($mensagensDefault as $key => $mensagem) { ?>
                                                <li>
                                                    <a> 
                                                        <span>
                                                            <span><?php echo $mensagem->usuario_criador->nome; ?> </span>
                                                        </span>
                                                        <span class="message"><?php echo $mensagem->mensagem; ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        <?php }else{ ?>
                                                <li>
                                                    <a> 
                                                        <span class="message"><?php echo "Sem mensagens para exibir"; ?></span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="right_col" role="main">
                <?php echo $this->fetch('content') ?>
            </div>
            <footer>
                <div class="pull-right">
                    ATIVO - SISTEMA GERENCIADOR DE ATIVIDADES COMPLEMENTARES
                </div>
                <div class="clearfix"></div>
            </footer>
        </div>
    </div>
    <?php echo $this->Html->script('custom') ?>
</body>
</html>
